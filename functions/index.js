/* eslint-disable promise/always-return */
/* eslint-disable promise/catch-or-return */
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mercadopago = require('mercadopago');
const axios = require('axios');
const sgMail = require('@sendgrid/mail');
const emailTemplate = require('./email.template');

const app = express();
admin.initializeApp();

app.use(cors({ origin: true }));
app.use(bodyParser.json());

if (process.env.FUNCTIONS_EMULATOR) {
  mercadopago.configurations.setAccessToken(functions.config().mercadopago.access_token_dev);
} else {
  mercadopago.configurations.setAccessToken(functions.config().mercadopago.access_token);
}
sgMail.setApiKey(functions.config().sendgrid.api_key);

app.get('/getInvoice', (req, res) => {
  const url = 'http://fluxcorpo2.dyndns.org:8080/webservices/ws/quotes/quote';
  axios
    .get(`${url}?folio=${req.query.folio}&taxId=${req.query.taxId}`)
    .then(response => {
      // handle success
      if (response.data) res.send(response.data);
      else {
        res.status(500).send('Error al consultar la cotizacion');
        console.log(res);
      }
      return response;
    })
    .catch(err => {
      // handle error
      console.log(err);
      res.status(400).send('Error al consultar la cotizacion');
    });
});

app.post('/makePayment', async (req, res, next) => {
  try {
    console.log(req.body);
    const { extras } = req.body;
    const token = req.body;
    delete token.extras;

    // make payment
    const pago = await mercadopago.payment.create(req.body);

    // save record
    extras.mpId = pago.response.id;
    extras.created = admin.firestore.Timestamp.now();
    console.log('Saving record...');
    await admin
      .firestore()
      .collection('pagos')
      .add(extras);
    console.log('Record saved');

    // send email
    const msg = {
      to: token.payer.email,
      from: 'facturacion@fluxometro.com',
      subject: `Recibo de pago de la cotización: ${extras.folio}`,
      text: token.description,
      html: emailTemplate(extras.folio, extras.name, extras.amount, pago.response.id),
    };
    await sgMail.send(msg);
    res.send('pago exitoso');
    return 'ok';
  } catch (err) {
    console.log('The error:', err);
    res.status(400).send(err);
    return next(err);
  }
});

exports.api = functions.https.onRequest(app);
