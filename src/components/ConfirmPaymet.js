import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Modal, Button } from 'react-bootstrap';
import BarLoader from 'react-spinners/BarLoader';
import { useToasts } from 'react-toast-notifications';
import axios from 'axios';

export default props => {
  const { addToast } = useToasts();
  const invoice = useSelector(state => state.invoice);
  const token = useSelector(state => state.token);
  const [isLoading, setIsLoading] = useState(false);
  const [response, setResponse] = useState(null);

  const handleClose = () => {
    props.onClose();
  };

  const sendPayment = () => {
    setIsLoading(true);
    props.onClose();
    // send data to server
    const url = process.env.REACT_APP_PAYMENT_URL;
    axios
      .post(url, token)
      .then(response => {
        console.log(response);
        setResponse(response.data);
      })
      .catch(error => {
        setIsLoading(false);
        addToast(`Hubo un error intenta de nuevo. (${error.response.data.message})`, {
          appearance: 'error',
          autoDismiss: true,
        });
        console.log(error.response);
      });
  };

  const reset = () => {
    window.location.reload();
  };

  return (
    <div>
      {isLoading && (
        <div className="loader">
          <div className="contain">
            {response ? (
              <div>
                Su pago fue recibido, gracias.
                <br />
                <Button onClick={reset}> Regresar</Button>
              </div>
            ) : (
              <div>
                <span>Realizando el pago por favor espere...</span>
                <BarLoader width={300} color={'#ec2031'} loading={true} />
              </div>
            )}
          </div>
        </div>
      )}

      <Modal
        show={props.show}
        onHide={handleClose}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title>Confirmar pago a factura: {invoice.folio}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Estas apunto de pagar <strong>{invoice.totalFormated}</strong>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Regresar
          </Button>
          <Button variant="primary" onClick={sendPayment}>
            Continuar y pagar
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
