import React from 'react';
import { useSelector } from 'react-redux';
import { Container, Card, Table } from 'react-bootstrap';

export default () => {
  const invoice = useSelector(state => state.invoice);
  return (
    <Container>
      <Card className="mb-4">
        <Card.Header>
          <strong>Cliente: </strong> {invoice.nombreCliente}
          <div className="float-right">
            <strong>Cotización:</strong> {invoice.folio}
          </div>
        </Card.Header>
        <Card.Body>
          <Table responsive>
            <thead>
              <tr>
                <th>Producto</th>
                <th>Unidad</th>
                <th>Cantidad</th>
              </tr>
            </thead>
            <tbody>
              {invoice.detalles.map(detalle => (
                <tr key={detalle.cotizacionDetalleId}>
                  <td>{detalle.productoDescripcion}</td>
                  <td>{detalle.unidad}</td>
                  <td>{detalle.cantidad}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card.Body>
        <Card.Footer>
          <div className="float-right">
            TOTAL: <strong>{invoice.totalFormated}</strong>
          </div>
        </Card.Footer>
      </Card>
    </Container>
  );
};
