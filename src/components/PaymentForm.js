import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container, Row, Col, Form, Button, Jumbotron } from 'react-bootstrap';
import Autocomplete from './Autocomplete';
import { useToasts } from 'react-toast-notifications';

import ConfirmPayment from './ConfirmPaymet';

const mp = window.Mercadopago;
mp.setPublishableKey(process.env.REACT_APP_PUBLIC_KEY);

function validateEmail(email) {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
}

const initValidations = {
  isInvalidCCNumber: false,
  isInvalidCCMonth: false,
  isInvalidCCCVC: false,
  isInvalidCardYear: false,
  isInvalidCCHolderName: false,
  isInvalidEmail: false,
  isInvalidPhone: false,
  isInvalidAddress: false,
};

export default () => {
  const { addToast } = useToasts();

  const dispatch = useDispatch();
  const invoice = useSelector(state => state.invoice);

  const [showConfirm, setShowConfirm] = useState(false);
  const [formFields, setFormFields] = useState({
    frmCCNum: '',
    cardExpirationMonth: '',
    cardExpirationYear: '',
    frmCCCVC: '',
    frmNameCC: '',
    email: '',
    phone: '',
    address: '',
  });
  const [validations, setValidations] = useState(initValidations);
  const [paymentMethod, setPaymentMethod] = useState('');

  // useEffect(() => {
  //   const total = props.invoice.total
  //     .toFixed(2)
  //     .toString()
  //     .split('.')
  //     .join('');
  //   setAmount(Dinero({ amount: Number(total), precision: 2 }).toUnit());
  // }, [props]);

  const handleOnChange = e => {
    const { id, value } = e.target;
    setFormFields({ ...formFields, [id]: value });
  };

  const validateCCNumber = e => {
    const val = e.target.value;
    if (val.length >= 6) {
      const bin = val.substring(0, 6);
      const opts = { bin };
      setValidations(ps => ({ ...ps, isInvalidCCNumber: false }));
      mp.getPaymentMethod(opts, (status, response) => {
        if (status === 200 && response) {
          const [data] = response;
          setPaymentMethod(data.id);
        } else {
          setValidations(ps => ({ ...ps, isInvalidCCNumber: true }));
          addToast('Numero de tarjeta invalido', { appearance: 'error', autoDismiss: true });
        }
      });
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    const form = e.currentTarget;

    // reset validations
    const fields = [];
    setValidations(initValidations);

    // check fields
    mp.createToken(form, (status, res) => {
      console.log(res);
      if (status === 400) {
        res.cause.forEach(cause => {
          if (cause.code === 'E301') {
            setValidations(ps => ({ ...ps, isInvalidCCNumber: true }));
            fields.push(false);
          }
          if (cause.code === '325' || cause.code === '326') {
            setValidations(ps => ({ ...ps, isInvalidCCMonth: true, isInvalidCardYear: true }));
            fields.push(false);
          }
          if (cause.code === 'E302') {
            setValidations(ps => ({ ...ps, isInvalidCCCVC: true }));
            fields.push(false);
          }
          if (cause.code === '221') {
            setValidations(ps => ({ ...ps, isInvalidCCHolderName: true }));
            fields.push(false);
          }
        });
      }

      if (!validateEmail(formFields.email)) {
        setValidations(ps => ({ ...ps, isInvalidEmail: true }));
        fields.push(false);
      } else setValidations(ps => ({ ...ps, isInvalidEmail: false }));

      if (formFields.address.length < 10) {
        setValidations(ps => ({ ...ps, isInvalidAddress: true }));
        fields.push(false);
      } else setValidations(ps => ({ ...ps, isInvalidAddress: false }));

      if (formFields.phone.length < 10) {
        setValidations(ps => ({ ...ps, isInvalidPhone: true }));
        fields.push(false);
      } else setValidations(ps => ({ ...ps, isInvalidPhone: false }));

      // check validations
      if (Object.values(fields).every(e => e)) {
        const token = {
          transaction_amount: invoice.total,
          token: res.id,
          description: `Pago de la factura: ${invoice.folio}`,
          installments: 1,
          payment_method_id: paymentMethod,
          payer: {
            email: formFields.email,
          },
          extras: {
            name: formFields.frmNameCC,
            phone: formFields.phone,
            address: formFields.address,
            amount: invoice.totalFormated,
            folio: invoice.folio,
          },
        };
        dispatch({ type: 'ADD_TOKEN', payload: token });
        setShowConfirm(true);
      } else {
        addToast('Revisa los campos en rojo', { appearance: 'error', autoDismiss: true });
      }
    });
  };

  const closeConfirm = () => {
    setShowConfirm(false);
  };

  return (
    <div>
      <Container>
        {/* <Alert variant="warning">
          Tarjeta de prueba: <strong>4075595716483764</strong>
        </Alert> */}
        <Jumbotron>
          <Form noValidate onSubmit={handleSubmit}>
            <Row>
              <Col>
                <Form.Group controlId="frmCCNum">
                  <Form.Label>Numero tarjeta</Form.Label>
                  <Form.Control
                    type="text"
                    data-checkout="cardNumber"
                    placeholder="Ingresa el numero de la tarjeta"
                    value={formFields.frmCCNum}
                    onChange={handleOnChange}
                    onBlur={validateCCNumber}
                    autoComplete="cc-number"
                    minLength={14}
                    maxLength={16}
                    isInvalid={validations.isInvalidCCNumber}
                    required
                  />
                </Form.Group>
              </Col>

              <Col lg={2}>
                <Form.Group controlId="cardExpirationMonth">
                  <Form.Label>Mes de expiración</Form.Label>
                  <Form.Control
                    type="text"
                    data-checkout="cardExpirationMonth"
                    placeholder="12"
                    maxLength="2"
                    onChange={handleOnChange}
                    value={formFields.cardExpirationMonth}
                    isInvalid={validations.isInvalidCCMonth}
                    required
                  />
                </Form.Group>
              </Col>

              <Col lg={2}>
                <Form.Group controlId="cardExpirationYear">
                  <Form.Label>Año de expiración</Form.Label>
                  <Form.Control
                    type="text"
                    data-checkout="cardExpirationYear"
                    placeholder="2025"
                    maxLength={4}
                    onChange={handleOnChange}
                    value={formFields.cardExpirationYear}
                    isInvalid={validations.isInvalidCardYear}
                    required
                  />
                </Form.Group>
              </Col>

              <Col lg={2}>
                <Form.Group controlId="frmCCCVC">
                  <Form.Label>Codigo de seguridad</Form.Label>
                  <Form.Control
                    type="text"
                    data-checkout="securityCode"
                    autoComplete="cc-csc"
                    placeholder="000"
                    onChange={handleOnChange}
                    value={formFields.frmCCCVC}
                    isInvalid={validations.isInvalidCCCVC}
                    minLength={3}
                    maxLength={4}
                    required
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group controlId="frmNameCC">
                  <Form.Label>Nombre</Form.Label>
                  <Form.Control
                    type="text"
                    data-checkout="cardholderName"
                    placeholder="Ingresa el nombre en la tarjeta"
                    autoComplete="cc-name"
                    minLength={8}
                    onChange={handleOnChange}
                    value={formFields.frmNameCC}
                    isInvalid={validations.isInvalidCCHolderName}
                    required
                  />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group controlId="email">
                  <Form.Label>Correo electrónico</Form.Label>
                  <Form.Control
                    type="email"
                    name="email"
                    placeholder="tunombre@email.com"
                    onChange={handleOnChange}
                    value={formFields.email}
                    isInvalid={validations.isInvalidEmail}
                    required
                  />
                </Form.Group>
              </Col>
            </Row>

            <Row>
              <Col>
                <Form.Group controlId="phone">
                  <Form.Label>Teléfono</Form.Label>
                  <Form.Control
                    type="phone"
                    name="phone"
                    placeholder="1245736598"
                    maxLength="10"
                    onChange={handleOnChange}
                    value={formFields.phone}
                    isInvalid={validations.isInvalidPhone}
                    required
                  />
                </Form.Group>
              </Col>
            </Row>

            <Autocomplete
              address={val => setFormFields({ ...formFields, address: val })}
              isInvalid={validations.isInvalidAddress}
            />

            {/*hidden fields*/}
            <input type="hidden" name="amount" id="amount" value={invoice.total} />
            <input type="hidden" name="description" />
            <Button variant="primary" type="submit" className="float-right">
              Pagar
            </Button>
          </Form>
        </Jumbotron>
      </Container>
      {/*  Confirm modal */}
      <ConfirmPayment onClose={closeConfirm} show={showConfirm} />
    </div>
  );
};
