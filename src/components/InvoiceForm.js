import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import Dinero from 'dinero.js';
import axios from 'axios';
import { Container, Button, Col, Form, InputGroup, Row } from 'react-bootstrap';
import BeatLoader from 'react-spinners/BeatLoader';
import { useToasts } from 'react-toast-notifications';

const toTitleCase = str => {
  if (str) {
    return str
      .trim()
      .split(' ')
      .map(x => x[0].toUpperCase() + x.slice(1).toLowerCase())
      .join(' ');
  } else {
    return str;
  }
};

export default () => {
  const { addToast } = useToasts();
  const [validated, setValidated] = useState(false);
  const [loading, setLoading] = useState(false);
  const [folio, setFolio] = useState(''); // PF024308
  const [rfc, setRfc] = useState('XAXX010101000');

  const dispatch = useDispatch();

  const saveInvoice = invoiceData => {
    // format data
    const total = invoiceData.total
      .toFixed(2)
      .toString()
      .split('.')
      .join('');
    invoiceData.nombreCliente = toTitleCase(invoiceData.nombreCliente);
    invoiceData.ejecutivo = toTitleCase(invoiceData.ejecutivo);
    invoiceData.bodega = toTitleCase(invoiceData.bodega);
    invoiceData.total = Dinero({ amount: Number(total), precision: 2 }).toUnit();
    invoiceData.totalFormated = Dinero({ amount: Number(total) }).toFormat('$0,0.00');
    dispatch({ type: 'ADD_INVOICE', payload: invoiceData });
  };

  const getInvoice = e => {
    e.preventDefault();
    setLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.preventDefault();
      e.stopPropagation();
    } else {
      const url = 'https://us-central1-flux-facturas.cloudfunctions.net/api/getInvoice';
      axios
        .get(`${url}?folio=${folio}&taxId=${rfc}`)
        .then(res => {
          console.log(res);
          if (res.data.localidad !== 'Cotización no encontrada') {
            saveInvoice(res.data);
          } else {
            addToast('No se encontro la cotización intenta de nuevo mas tarde', {
              appearance: 'error',
              autoDismiss: true,
            });
          }
          setLoading(false);
        })
        .catch(err => {
          setLoading(false);
          console.error(err);
        });
    }
    setValidated(true);
  };

  return (
    <Container>
      <Form noValidate validated={validated} onSubmit={getInvoice}>
        <Row className="mt-4">
          <Col>
            <Form.Group controlId="folio">
              <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text>Folio:</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                  type="text"
                  value={folio}
                  onChange={e => setFolio(e.target.value)}
                  placeholder="Ingresa el folio de la factura"
                  disabled={loading}
                  required
                />
                <Form.Control.Feedback type="invalid">Folio invalido</Form.Control.Feedback>
              </InputGroup>
            </Form.Group>
          </Col>

          <Col>
            <Form.Group controlId="rfc">
              <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text>RFC:</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                  type="text"
                  value={rfc}
                  onChange={e => setRfc(e.target.value)}
                  placeholder="Ingresa el RFC"
                  disabled={loading}
                  required
                />
                <Form.Control.Feedback type="invalid">RFC invalido</Form.Control.Feedback>
              </InputGroup>
            </Form.Group>
          </Col>

          <Col lg={3}>
            <Button variant="primary" type="submit" disabled={loading} block>
              {loading ? (
                <BeatLoader size={10} color={'#fff'} loading={true} />
              ) : (
                <span>Obtener Cotización</span>
              )}
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
