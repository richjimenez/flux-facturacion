import React, { useState } from 'react';
import PlacesAutocomplete, { geocodeByAddress } from 'react-places-autocomplete';
import { Col, Form, Row } from 'react-bootstrap';

export default props => {
  const [address, setAddress] = useState('');

  const handleChange = addr => {
    setAddress(addr);
    props.address(addr);
  };

  const handleSelect = address => {
    geocodeByAddress(address)
      .then(results => {
        const [result] = results;
        setAddress(result.formatted_address);
        props.address(result.formatted_address);
      })
      .catch(error => console.error('Error', error));
  };

  return (
    <PlacesAutocomplete value={address} onChange={handleChange} onSelect={handleSelect}>
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <Row>
          <Col>
            <Form.Group controlId="address">
              <Form.Label>Dirección de envió</Form.Label>
              <Form.Control
                {...getInputProps({
                  placeholder: 'Ingresa la dirección de envió',
                  className: 'location-search-input',
                })}
                value={address}
                isInvalid={props.isInvalid}
                required
              />
              <div className="autocomplete-dropdown-container">
                {loading && <div>Loading...</div>}
                {suggestions.map(suggestion => {
                  const className = suggestion.active
                    ? 'suggestion-item--active'
                    : 'suggestion-item';
                  // inline style for demonstration purpose
                  const style = suggestion.active
                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                  return (
                    <div
                      {...getSuggestionItemProps(suggestion, {
                        className,
                        style,
                      })}
                    >
                      <span>{suggestion.description}</span>
                    </div>
                  );
                })}
              </div>
            </Form.Group>
          </Col>
        </Row>
      )}
    </PlacesAutocomplete>
  );
};
