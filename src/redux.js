const INIT_STATE = { invoice: null, token: null };

const reducer = (state = INIT_STATE, { type, payload }) => {
  switch (type) {
    case 'ADD_INVOICE':
      return {
        ...state,
        invoice: payload,
      };
    case 'ADD_TOKEN':
      return {
        ...state,
        token: payload,
      };
    default:
      return state;
  }
};

export { reducer };
