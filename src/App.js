import React from 'react';
import { useSelector } from 'react-redux';
import * as firebase from 'firebase/app';
import 'firebase/analytics';
import Banner from './banner.png';
import Logo from './logo.png';

import InvoiceForm from './components/InvoiceForm';
import PaymentForm from './components/PaymentForm';
import Invoice from './components/Invoice';
import Footer from './components/Footer';

import './App.scss';

const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};

firebase.initializeApp(firebaseConfig);
firebase.analytics();

function App() {
  const invoice = useSelector(state => state.invoice);
  return (
    <div className="app">
      <header>
        <div className="logo">
          <img src={Logo} alt="El Fluxometro" className="img-fluid" />
        </div>
        <div className="black-line" />
        <div className="banner">
          <img src={Banner} alt="Ahora pagar es mas facil" className="img-fluid" />
        </div>
      </header>

      <main>
        <InvoiceForm />
        {invoice && (
          <div>
            <Invoice />
            <PaymentForm invoice={invoice} />
          </div>
        )}
      </main>

      <Footer />
    </div>
  );
}

export default App;
